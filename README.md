# ansible-docker-testenvironment

## Simple environment for quickly setup and work with ansible in a docker environment for testing/learning 

To ensure a repetable consistant environment for testing ansible installation, I created a couple of generic docker images, that I 
then started using the MyEnvironmentForAnsible docker-compose.yml to ensure it was possible to reach the "host systems"

